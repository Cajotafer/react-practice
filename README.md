Pera iniciar un proyecto hay dos formas.
1. Usando un boilerplate, que básicamente es una estructura de archivos o andamiaje base para iniciar.
En este caso, deberíamos intalar:
```
npm install -g create-react-app
```
2. La segunda opción es armando el proyecto desde cero, para lo cual es recomendable usar webPack. 
**Una vez configurado el package.json que genera "npm init" y webPack con sus dependencias, se procede a instalar las dependencias de React.**

```
npm install react react-dom --save

// react se encarga de poder juntar los diversos componentes como legos
// react-dom es el responsable de renderizar o mostrar esto de alguna manera

// --save agrega la dependencia directamente
// -dev agrega la dependincia de desarrollo
```

Cuando las dependencias de React están instaladas ya podemos darle uso, esto lo hacemos en el archivo .js pricipal con: 

```
import React from 'react';
import ReactDOM from 'react-dom';
```

Al final debemos ejecutar el método **render()** de ReactDOM que recibirá como parámetros:

1. Que voy a renderizar: puede ser un elemento o componente de React. Por ejemplo podemos agregar:

```
const paraRenderizar = <h1>Hola mundo</h1>
```

2. Donde renderizaré: Tiene que ser un lugar en el DOM que ya exista. 
Para esto, solemos crear un elemento en el html con el **id="app"**, donde harémos el renderizado, posteriormente lo llamamos y asociamos a una constante que podamos usar en js.

```
const app = document.getElementById('app')
```

El método se ejecutaría de esta forma con los parámetros ya definidos:

```
ReactDOM.render(paraRenderizar, app)
```

Ahora, para ejecutar y probar, ingresamos en cónsola: 
```
npm run build:prod 

o

npm run build:dev
```

Según lo que tengamos configurado como scripts dentro del package.json

---

React trabaja por componentes que son estos conjuntos de elementos reutilizables.
Existen 3 tipos:

- Funcionales
- Puros
- De estado

La organización es algo muy importante, por esto se suele crear un nuevo directorio **src** en el directorio principal del proyecto, dentro del **src** se pueden dividir los componentes en base a **layouts** Ej:

```
src
--playlist
----components
------media.js
--header
----components
--footer
----components
```

Tomando este ejemplo iniciaremos creando el componente media, que básicamente será una imagen con descripción.

Para iniciar un componente RECUERDA siempre importar React.

```
import React from 'react';
```

Cada componente generará una nueva clase con el nombre del componente, que extiende de **React.Component**. De esta manera, literalmente estamos creando un nuevo Componente heredando todas las características propias de un componente de React.

Entre esas características de React.Component, existe un método principal que es **render()** y que retorna la **UI**.

```
class Media extends React.Component {
    render() {
        return (
            <div>
                <figure>
                    <img
                        src=""
                        alt=""
                        width={260}
                        height={160}
                    />
                </figure>
                <h3>Este es el titulo del componente</h3>
                <p>Lorem ipsum algo mas va aqui estoy seguro de que es asi</p>
            </div>
        )
    }
}
```

Luego, este componente debe ser ser llamado desde el archivo principal que renderiza con el ReactDOM.
Para esto primero debemos **exportar** el componente agregando al final.

```
export default Media;
```

Queda finalmente:

```
import React from 'react';

class Media extends React.Component {
    render() {
        return (
            <div>
                <figure>
                    <img
                        src=""
                        alt=""
                        width={260}
                        height={160}
                    />
                </figure>
                <h3>Este es el titulo del componente</h3>
                <p>Lorem ipsum algo mas va aqui estoy seguro de que es asi</p>
            </div>
        )
    }
}

export default Media;
```

---

Ahora este componente si puede ser importado desde el .js principal, agregando esto:

```
import Media from './src/playlist/components/Media';
```

Donde **Media** es el nombre que quieres darle al  contenedor de este componente en este nuevo contexto.

Luego de ser importado el componente, debe ser renderizado.

IMPORTANTE si quieres renderizar un componente con **ReactDOM.render()** el nombre del componente debe ser tratado como un identificador html, entre " <Componente/> ".

```
ReactDOM.render(<Media/>, app);
```

El js principal deberia quedar asi:

```
import React from 'react';
import ReactDOM from 'react-dom';
import Media from './src/playlist/components/Media';

const app = document.getElementById('app')

ReactDOM.render(<Media/>, app);
```


---
*TIP: Gracias a la destructuracion de js, cuando se hace **import React from 'react'**, se puede también importar de una vez **Component** así:*

```
import React, {Component} from 'react';
```

De esta forma puedes cambiar:

```
class Media extends React.Component {...}
```
por:
```
class Media extends Component {...}
```
Lo mismo se puede hacer en el .js principal, donde se puede cambiar:

```
import ReactDOM from 'react-dom';
```

por:

```
import { render } from 'react-dom';
```

Para poder usar al final directamente

```
render(<Media/>, app)
```

en lugar de 

```
ReactDOM.render(...)
```
---

## Estilos en React

Una forma de agregar estilos es prácticamente como "inline". Para esto, en el mismo componente, al elemento deseado le agregarémos la propiedad **style**.

```
<div styles={...}></div>
```

Ahora, en este caso los estilos deberán ser definidos como un objeto, comportandose de la siguiente manera:

```
class Media extends Component {
    render() {
		const styles = {
			container: {
				color: '#4456b',
				backgroundColor: 'red',
				width: 260,
			}
		}

        return (
            <div style={styles.container}>
                <figure>
                    <img
                        src=""
                        alt=""
                        width={260}
                        height={160}
                    />
                </figure>
                <h3>Este es el titulo del componente</h3>
                <p>Lorem ipsum algo mas va aqui estoy seguro de que es asi</p>
            </div>
        )
    }
}
```

La otra forma es importando el archivo .css al propio componente.

```
import './media.css';
```

Así cada componente tendrá también sus propios estilos enlazados. Pero muy importante, para asignar las clases en jsx no puedes usar la palabra reservada **class**, debes usar **className**.

```
import React, { Component } from 'react';
import './media.css';

class Media extends Component {
    render() {
        return (
            <div className='Media'>
                <figure>
                    <img className='Media-image Media-cover'
                        src="./images/covers/bitcoin.jpg"
                        alt=""
                        width={260}
                        height={160}
                    />
                </figure>
                <h3 className='Media-title'>Este es el titulo del componente</h3>
                <p className='Media-author'>Carlos Fernandez</p>
            </div>
        )
    }
}

export default Media;
```

Esta forma en la que hemos agregado atributos, que en React realmente son llamados **propiedades** puede hacerse también de forma dinámica. En este caso, las propiedades se envían al momento del renderizado, es decir, en el archivo js principal, en el método **render()**. Ej:

```
render(<Media 
    image="./images/covers/bitcoin.jpg"
    title="Este es el titulo" 
    author="Cajotafer"
    />, app);
```

Una vez que estas declarando que el elemento se va a renderizar y que cuando se renderize recibirá estas propiedades, puedes acceder a las propiedades desde **this.props**, dentro del componente ya que al final, el componente es un prototipo y tiene su propio contexto.

Una buena práctica es desestructurar el objeto que recibe:

```
const { image, title, author} = this.props;
```

Y así resulta más fácil manipular las variables.

```
class Media extends Component {
    render() {
        const { image, title, author} = this.props;

        return (
            <div className='Media'>
                <figure>
                    <img className='Media-image Media-cover'
                        src={image}
                        alt=""
                        width={260}
                        height={160}
                    />
                </figure>
                <h3 className='Media-title'>{title}</h3>
                <p className='Media-author'>{author}</p>
            </div>
        )
    }
}
```

---

## Validando tipado en propiedades

Para validar las propiedades que reciben nuestros componentes, es necesario instalar una nueva dependencia.

**npm install prop-types --save**

Una vez que la dependencia está instalada es necesario importarla en la dependencia donde se le piense dar uso.

```
import PropTypes from 'prop-types';
``` 

Justo al final del componente, antes de exportarlo con **export default Componente;** vamos a definir el PropTypes de cada propiedad que va a recibir el componente, de la siguiente manera:

```
Componente.propTypes = {
	this.props.propiedad1: PropTypes.boolean,
	this.props.propiedad2: PropTypes.number,
}
```

Con nuestro ejemplo se veria de la siguiente manera

```
Media.propTypes = {
	image: PropTypes.string,
	title: PropTypes.string,
	author: PropTypes.string,
}
```

Otros tipos de PropTypes son: **array - func - object - number - string - oneOf(array) - bool - symbol**

PropTypes.oneOf(array) es perfecto para cuando quieres validar el contenido exacto de la propiedad. Puedes definir los valores aceptados para la propiedad, en forma de arreglo.

```
Media.propTypes = {
	type: PropTypes.oneOf(['audio','video']),
}
```
Tambien podemos hacer que la propiedad sea obligatoria agregando **.isRequired**. Asi, si la propiedad lleva vacía

```
Media.propTypes = {
	image: PropTypes.string.isRequired,
	title: PropTypes.string.isRequired,
	author: PropTypes.string,
}
```

## Enlazando eventos del DOM

Básicamente asignas **onEvent** al elemento html y lo enlazas a una función. Esta función debe ser llamada con **this** si está definida dentro de nuestra misma clase.

```
<div onClick={this.handleClick}></div>
```

Ahora, si queremos que el this de esta función apunte al mismo de la clase, es necesario hacerle un **bind()**, esto lo debemos hacer en el **constructor** de la clase.

```
class Media extends Component {
	constructor(props) {
		super(props)
		this.funcion = this.funcion.bind(this)
	}
}
```

De hesta forma ahora podemos usar las propiedades en esta funcion **handleClick** porque le dimos la referencia de contexto de la clase.

MEJOR OPCION en lugar de todo esto, es usar una arrow function, asi no tenemos que hacer **bind** de this porque la arrow function hereda siempre el contexto.

```
// Eliminamos el constructor en este caso y la funcion queda asi

handleClick = (event) => {
	console.log(this.props.title)
}
```

## Estados de los componentes

Las propiedades que reciben los componentes son inmutables, es decir, no pueden ser modificados, sin embargo, tenemos una opción. Los estados.

Para asignar estados a un componente se puede hacer de dos formas:

1. De clarandolo en el constructor de la clase

```
constructor(props) {
	super(props)
	this.state = {
		propiedad: 'valor',
		propiedad2: 'valor',
	}
}
```

2. Declarandolo sin constructor (ES7) tan pronto inicia la clase

```
state = {
	propiedad: 'valor',
	propiedad2: 'valor',
}
```

Ahora el acceso a las propiedades quedaría de la siguiente manera con el metodo **setState()** de Component, o sea, debe estar con el contexto de la clase "**this**":

```
this.setState({
	propiedad: 'nuevoValor',
	propiedad2: 'nuevoValor',
})
```

## Ciclo de vida de un componente

---

## 

Si tienes un componente que espera un objeto con propiedades, cuando le envias las propiedades puedes enviar una replica de un documento.

```
import data from './data.json';

...
...

render(<Componente {...data}/>, app)
```

```
data = {
	image: 'url',
	title: 'title',
	desc: 'description'
}
```
Por lo tanto **this.props** del componente sera un objeto identico a **data**.

## Tipos de componentes

- **Components** : normal
- **PureComponents** : shouldComponentUpdate lo evita por defecto, cuando sabemos que nuestro componente no tendrá actualizaciones, es mejor usar este tipo de componente.
- **Componente funcional** : es simplemente una funcion que recibe como parámetros **(props)**. No tiene ciclo de vida. Excelente para componentes que solo haran impresión de UI.




- 

## Ciclo de vida de los componentes
- constructor()
Enlazo (bind) eventos y/o inicializo estado

- componentWillMount()
Se ejecuta antes de montar el componente
Se podría usar para hacer un setState()

- render()
Contiene todos los elementos a renderizar
podrías usarlo para calcular propiedades (ej: concatenar una cadena)

- componentDidMount()
Solo se lanza una vez
Ideal para llamar a una API, hacer un setInteval, etc

**Actualización**

- componentWillReceiveProps()
Es llamado cuando el componente recibe nuevas propiedades.

- shouldComponentUpdate()
Idea para poner una condición y  si las propiedades que le llegaron anteriormente
eran las mismas que tenia retornar false para evitar re-renderear el componente

- componentWillUpdate()
metodo llamado antes de re-renderizar el componente si shouldComponentUpdate devolvió true

**re-render si es necesario...**

- componentDidUpdate()
Método llamado luego del re-render

- componentWillUnmount()
Método llamado antes de desmontar el componente

- componentDidCatch()
Si ocurre algún error, lo capturo desde acá:


**Los métodos de los Componentes son llamados en momentos específicos:**

- constructor cuando se crea el componente (como siempre)
- componentDidMount cuando el componente es agregado al DOM
- componentWillMount durante la renderización en el servidor
- componentWillUnmount cuando el componente está siendo eliminado
- shouldComponentUpdate luego de que el estado (state) o las propiedades (props) del componente han sido modificadas.
- componentWillUpdate y componentDidUpdate antes y después de que el componente se re-renderice.
- componentWillReceiveProps antes de que el componente haya recibido propiedades (props) cuto valor haya cambiado

## Smart Components y Dumb Componentes

**Presentacional** Cómo se ve

- Puede contener smart components u otros componentes de UI
- Permiten composición con `[props.children]``
- No depeden del resto de la aplicación
- No especifica cómo los datos son cargados o mutados
- Recibe datos y callbacks solo con propiedades
- Rara vez tienen su propio estado
- Están escritos como componentes funcionales a menos que necesiten mejoras de performance. Sólo pueden ser Componentes funcionales o Pure Components

**Containers** Qué hace

- Concetrado en el funcionamiento de la aplicación
- Contienen componentes de UI u otros containers
- No tienen estilos
- Proveen de datos a componentes de UI u otros contenedores
- Proveen de callbacks a la UI
- Normalmente tienen estado
- Llaman acciones
- Generados por higher order components

## Widgets fuera del elemento root

Esto es algo muy usado para los modales, tooltips y cualquier otro tipo de ventana emergente que se renderiza fuera del DOM inicialmente.

En primer lugar, en el html se crea un nuevo elemento que es donde se va a renderizar el portal.

```
<div id=app></div>
<div id=modal></div>
```

Ahora debemos crear la clase **container** de nuestro modal que, en este caso, estará contenida en nuestro container principal.

```
import ModalContainer from './modal-container';

class Home extends Component {
    render(){
        return (
            <HomeLayout data={this.props}>
                <Related/>
                <Categories categories={this.props.data.categories}/>
                <ModalContainer/>     //MODAL CONTAINER
            </HomeLayout>
        )
    }
}
```
Si damos una vistazo, la magia ocurre el componente del modal:

```
import React, { Component } from 'react';
import { createPortal } from 'react-dom';

class ModalContainer extends Component {
    render() {
        return createPortal(
            this.props.children, 
            document.getElementById('modal-container')
        )
    }
}

export default ModalContainer
```

Primero, aparte de lo cotidiano tomando en cuenta que esto es un componente más, también importamos **react-dom** ya que este tiene el método **createPortal(child, container)**. Donde child es lo que va a renderizar y container es donde lo va a renderizar. El **render()** va a retornar la función **createPortal**.


## Capturando errores con componentDidCatch()

Con React puedes capturar cuando ocurre un error con **componentDidCatch()** que puede recibir como parámetros el **error** y la **info** del error.

Lo ideal es crear un componente que maneje los errores, este componente tendrá un **estado inicial de error: false** que cambiará a **true** en **componentDidCatch()**. Por tanto, cuando el estado de error sea true retornará el layout del error correspondiente (debería existir alguna forma de imprimir un layout en base al tipo de error ocurrido). En caso de que no exista error, retorna **this.props.children**

Creando el componente **HandleError**

```
import React, { Component } from 'react';
import RegularError from '../components/regular-error';

class HandleError extends Component {
    state = {
        handleError: false,
    }

    componentDidCatch(error, info) {
        this.setState({
            handleError: true,
        })
    }
    
    render() {
        if (this.state.handleError) {
            return (
                <RegularError/>
            )
        }
        return this.props.children;
    }
}

export default HandleError
```

En este caso este componente inteligente retornará el componente **RegularError** que simplemente contiene la UI del error que le corresponde.

Entonces, cuando queremos que algo tenga este manejo de errores, simplemente contenemos ese elemento en el **<HandleError>**

```
render(){
    return (
        <HandleError>
            <HomeLayout data={this.props}>
                <Related/>
                <VideoPlayer/>
                <Categories 
                    categories={this.props.data.categories}
                    handleOpenModal={this.handleOpenModal}
                />
                {
                    this.state.showModal &&
                        <ModalContainer>
                            <Modal
                                handleClick={this.handleCloseModal}
                            >
                                <h1>Esto es un portal</h1>
                            </Modal>
                        </ModalContainer>
                }
            </HomeLayout>
        </HandleError>
    )
}
```




## Formularios

Recuerda que para evitar la recarga de la página luego de un submit puedes usar **event.preventDefault()**.

La idea es que los inputs tengan una referencia propia, para hacer esto, al elemento **input** le agregamos una propiedad **ref** que recibirá una función desde su controlador.

```
 <input 
    ref={props.setRef}
    type="text" 
    placeholder="Busca tu video favorito"
    className="Search-input"
/>
```

Esta función **setRef** la enviamos desde el contenedor como una propiedad y lo que hará será relacionar el elemento HTML (recibido en la función como un parámetro) a un espacio en memoria de **this**, en este caso lo llamamos input.

```

class SearchContainer extends Component {

    ...

    setSearchRef = (element) => {
        this.input = element;
    }

    render() {
        return (
            <Search
                setRef={this.setSearchRef}
                handleSubmit={this.handleSubmit}
            />
        )
    }
}
```

Si al input se le asigna un value por defecto, no puede hacerse con **value=""**, en el caso de React debe hacerse con **defaultValue=""**. De esta forma el campo si podrá ser modificado.

Si necesitamos recibir un valor por defecto de forma dinámica o simplemente necesitamos hacer una modificación en tiempo real del texto agregado al input, podemos agregar un estado en el container para nuestro input. Este estado puede cambiar con el evento **onChange** de nuestro input, que llamará una función como propiedad y tendrá adicionalmente el value que recibirá como propiedad.

```
 <input 
    ref={props.setRef}
    type="text" 
    placeholder="Busca tu video favorito"
    className="Search-input"
    
    onChange={props.handleChange}
    value={props.value}
/>
```

**handleChange** es una función que seteará un nuevo estado y el estado será el valor. Viendo esto en el componente controlador:

```
state = {
    value: 'Por defecto'
}

handleValueChange = (event) => {
    this.setState({
        value: event.target.value.replace(' ', '-')
    })
}

render() {
    return (
        <Search
            setRef={this.setSearchRef}
            handleSubmit={this.handleSubmit}
            
            handleChange={this.handleValueChange}
            value={this.state.value}
        />
    )
}
```

## Eventos en React 

https://reactjs.org/docs/events.html

## Paso de propiedades desde un hijo dinamico al padre

Imagina que tenemos un componente llamado **padre**, otro **hijo** y un **tio**.
El padre crea distintos hijos con propiedades independientes por hijo y el tío necesita al menos 1 o 2 datos de cada hijo. Así que necesita que cada hijo, de forma individual, le haga saber su propia información.

En este caso, el hijo ejecuta una función **handleClick** que ha recibido como propiedad desde su padre pero a la vez necesita indicarle al padre cual hijo ha sido el que ha ejecutado la función, para eso podemos enviar de vuelta las propiedades al padre.

```
class Media extends Component {

    handleClick = (event) => {
        this.props.handleClick(this.props)
    }

    render() {

        const { cover, title, author } = this.props

        return (
            <div 
                className='Media' 
                onClick={this.handleClick}
            >
   ...
   ...
}
```
En este caso, en lugar de ejecutar la función **handleClick** que ha recibido como propiedad, ejecuta su propia **handleClick** que recibe el evento y esta función es la que ejecuta el handleClick del padre, enviando como parámetro las propiedades del mismo elemento **this.props**.

De esta forma cuando se ejecuta la función inicial en el padre, recibe como parámetro las propiedades que identifican al hijo y puede asignarle un estado.

```
handleOpenModal = (media) => {
    this.setState({
        showModal: true,
        media
    })
}
```
