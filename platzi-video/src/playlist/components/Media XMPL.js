import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './media.css';

class Media extends Component {
/*     constructor(props) {
        super(props)
        this.state = {
            image: this.props.image,
            title: this.props.title,
            author: this.props.author,
        }
        this.handleClick = this.handleClick.bind(this)
    }

    handleClick(event) {
        console.log(this.props.title)
    } */

    state = {
        image: this.props.image,
        title: this.props.title,
        author: this.props.author,
        isToggled: false,
    }

    handleClick = (event) => {
        if (this.state.isToggled) {
            this.setState({
                image: this.props.image,
                title: this.props.title,
                author: this.props.author,
                isToggled: false,
            })
        } else {
            this.setState({
                image: './images/covers/despacito.jpg',
                title: 'Nuevo titulo para los panas',
                author: 'Piolin',
                isToggled: true,
            })
        }
    }

    render() {
        let { image, title, author } = this.state

        return (
            <div className='Media' onClick={this.handleClick}>
                <figure>
                    <img 
                        className='Media-image Media-cover'
                        src={image}
                        alt=""
                        width={260}
                        height={160} 
                    />
                </figure>
                <h3 className='Media-title'>{title}</h3>
                <p className='Media-author'>{author}</p>
            </div>
        )
    }
}

Media.propTypes = {
    image: PropTypes.string,
    title: PropTypes.string,
    author: PropTypes.string,
    type: PropTypes.oneOf(['video','audio']),
}

export default Media;