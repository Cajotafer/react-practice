import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './media.css';

class Media extends Component {

    handleClick = (event) => {
        this.props.handleClick(this.props)
    }

    render() {

        const { cover, title, author } = this.props

        return (
            <div 
                className='Media' 
                onClick={this.handleClick}
            >
                <figure>
                    <img 
                        className='Media-cover'
                        src={cover}
                        alt=""
                        width={260}
                        height={160} 
                    />
                </figure>
                <h3 className='Media-title'>{title}</h3>
                <p className='Media-author'>{author}</p>
            </div>
        )
    }
}

Media.propTypes = {
    cover: PropTypes.string,
    title: PropTypes.string,
    author: PropTypes.string,
    type: PropTypes.oneOf(['video','audio']),
}

export default Media;