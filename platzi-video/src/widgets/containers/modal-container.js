import React, { Component } from 'react';
import { createPortal } from 'react-dom';

const modalRoot = document.getElementById('modal-container')

class ModalContainer extends Component {

    render() {
        return createPortal(
            this.props.children, 
            modalRoot
        )
    }

/*     componentDidMount() {
        modalRoot.appendChild(this.props.children)
    }

    componentWillUnmount() {
        modalRoot.removeChild(this.props.children)
    }   */
}

export default ModalContainer