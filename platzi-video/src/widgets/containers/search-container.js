import React, { Component } from 'react';
import Search from '../components/search';

class SearchContainer extends Component {

    state = {
        value: 'Por defecto'
    }

    handleSubmit = (event) => {
        event.preventDefault()

        if (this.input.value) {
            console.log(`Nueva búsqueda: ${this.input.value}`)
            this.input.value = ''
        }
    }

    setSearchRef = (element) => {
        this.input = element;
    }

    handleValueChange = (event) => {
        this.setState({
            value: event.target.value.replace(' ', '-')
        })
    }

    render() {
        return (
            <Search
                setRef={this.setSearchRef}
                handleSubmit={this.handleSubmit}
                handleChange={this.handleValueChange}
                value={this.state.value}
            />
        )
    }
}

export default SearchContainer