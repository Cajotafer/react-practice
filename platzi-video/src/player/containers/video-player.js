import React, { Component } from 'react';
import VideoPlayerLayout from '../components/video-player-layout';
import Video from '../components/video';
import Title from '../components/title';
import PlayPause from '../components/play-pause';
import Timer from '../components/timer';
import Controls from '../components/video-player-controls';
import { formattedTime } from '../components/utilities';
import ProgressBar from '../components/progress-bar';
import Spinner from '../components/spinner';
import Volume from '../../icons/components/Volume';
import FullScreen from '../../icons/components/fullscreen';

class VideoPlayer extends Component {
    state = {
        pause: true,
        duration: 0,
        currentTime: 0,
        durationFloat: 0,
        currentTimeFloat: 0,
        loading: true,
        currentVolume: .8,
    }
    
    togglePlay = (event) => {
        this.setState({
            pause: !this.state.pause,
        })
    }

    componentDidMount() {
        this.setState({
            pause: (!this.props.autoplay),
        })
    }

    handleLoadedMetadata = (event) => {
        this.video = event.target
        this.setState({
            duration: formattedTime(this.video.duration),
            durationFloat: this.video.duration,
        })
    }

    handleTimeUpdate = (event) => {
        this.setState({
            currentTime: formattedTime(this.video.currentTime),
            currentTimeFloat: this.video.currentTime,
        })
    }

    handleProgressChange = (event) => {
        this.video.currentTime = event.target.value
    }

    handleSeeking = (event) => {
        this.setState({
            loading: true,
        })
    }

    handleSeeked = (event) => {
        this.setState({
            loading: false,
            pause: false,
        })
    }

    handleReady = (event) => {
        this.setState({
            loading: false,
        })
    }

    handleVolumeChange = (event) => {
        this.video.volume = event.target.value
        this.setState({
            currentVolume: this.video.volume,
        })
    }

    toggleMute = (event) => {
        if (this.video.volume !== 0){
            this.lastVolume = this.video.volume
            this.video.volume = 0;
            this.setState({
                currentVolume: this.video.volume,
            })
        } else {
            this.video.volume = this.lastVolume
            this.setState({
                currentVolume: this.video.volume,
            })
        }
    }

    handleFullScreen = (event) => { 
        // Toma en cuenta que estos métodos cambian sus prefijos dependiendo del navegador
        if (!document.webkitIsFullScreen){
            this.player.webkitRequestFullscreen()
        } else {
            document.webkitExitFullscreen()
        }
    }

    setRef = (element) => {
        this.player = element
    }

    render() {
        return (
            <VideoPlayerLayout
                setRef={this.setRef}
            >
                <Title
                    title = {this.props.title}
                />
                <Controls>
                    <PlayPause
                        pause = {this.state.pause}
                        handleClick = {this.togglePlay}
                    />
                    <Volume
                        size = {20}
                        color = "white"
                        handleVolumeChange = {this.handleVolumeChange}
                        toggleMute = {this.toggleMute}
                        value = {this.state.currentVolume}
                    />
                    <ProgressBar
                        duration = {this.state.durationFloat}
                        value = {this.state.currentTimeFloat}
                        handleProgressChange = {this.handleProgressChange}
                    />
                    <Timer
                        duration = {this.state.duration}
                        currentTime = {this.state.currentTime}
                    />
                    <FullScreen
                        size = {20}
                        color = "white"
                        handleFullScreen = {this.handleFullScreen}
                    />
                </Controls>
                <Spinner
                    active = {this.state.loading}
                />
                <Video
                    src={this.props.src}
                    autoplay = {this.props.autoplay}
                    pause = {this.state.pause}
                    handleLoadedMetadata = {this.handleLoadedMetadata}
                    handleTimeUpdate = {this.handleTimeUpdate}
                    handleSeeking = {this.handleSeeking}
                    handleSeeked = {this.handleSeeked}
                    handleReady = {this.handleReady}
                />
            </VideoPlayerLayout>
        )
    }
}

export default VideoPlayer