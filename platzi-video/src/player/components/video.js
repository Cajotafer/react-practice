import React, { Component } from 'react';
import './video.css';

class Video extends Component {
    togglePlay() {
        this.props.pause ?
            this.video.play()
        :
            this.video.pause()
    }

    setRef = (element) => {
        this.video = element
    }

    componentWillReceiveProps(nextProps){
        if (nextProps.pause !== this.props.pause){
            this.togglePlay();
        }
    }

    render() {
        const {
            handleLoadedMetadata,
            handleTimeUpdate,
            handleSeeking,
            handleSeeked,
            handleReady,
        } = this.props

        return (
            <div className="Video">
                <video
                    autoPlay={this.props.autoplay}
                    src={this.props.src}
                    ref={this.setRef}
                    onLoadedMetadata={handleLoadedMetadata}
                    onTimeUpdate={handleTimeUpdate}
                    onSeeking={handleSeeking}
                    onSeeked={handleSeeked}
                    onCanPlayThrough = {handleReady}
                />
            </div>
        )
    }
}

export default Video